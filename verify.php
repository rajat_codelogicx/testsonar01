<!DOCTYPE html>
<html>
<body>

<h1>Developer News</h1>

<?php echo "The Best PHP Examples"; ?>

</body>
</html> 

<?php

// Assign the value "Hello!" to the variable "greeting"
$greeting = "Hello!";
// Assign the value 8 to the variable "month"
$month = 8;
// Assign the value 2019 to the variable "year"
$year = 2019;

class Car {
    function Car() {
        $this->model = "Tesla";
    }
}

// create an object
$Lightning = new Car();

// show object properties
echo $Lightning->model;

// prints: mysql link
$c = mysql_connect();
echo get_resource_type($c) . "\n";

// prints: stream
$fp = fopen("foo", "w");
echo get_resource_type($fp) . "\n";

// prints: domxml document
$doc = new_xmldoc("1.0");
echo get_resource_type($doc->doc) . "\n";

echo strrev("Developer News"); // outputs sweN repoleveD

define("freeCodeCamp", "Learn to code and help nonprofits", false);
echo freeCodeCamp;

echo 1 <=> 1; // 0
echo 1 <=> 2; // -1
echo 2 <=> 1; // 1

//initialize with a random integer within range
$diceNumber = mt_rand(1, 6);

//initialize
$numText = "";

//calling switch statement
  switch($diceNumber) 
  {
  case 1:
    $numText = "One";
    break;
  case 2:
    $numText = "Two";
    break;
  case 3:
  case 4:
    // case 3 and 4 will go to this line
    $numText = "Three or Four";
    break;
  case 5:
    $numText = "Five";
    echo $numText;
    // break; //without specify break or return it will continue execute to next case.
  case 6:
    $numText = "Six";
    echo $numText;
    break;
  default:
    $numText = "unknown";
  }
  
  //display result
  echo 'Dice show number '.$numText.'.';

  $index = 10;
while ($index >= 0)
{
    echo "The index is ".$index.".\n";
    $index--;
}

function say_hello() {
    return "Hello!";
  }
  
  echo say_hello();

  function makeItBIG($a_lot_of_names) {
    foreach($a_lot_of_names as $the_simpsons) {
      $BIG[] = strtoupper($the_simpsons);
    }
    return $BIG;
  }
  
  $a_lot_of_names = ['Homer', 'Marge', 'Bart', 'Maggy', 'Lisa'];
  var_dump(makeItBIG($a_lot_of_names));

  $students = 
  array(
    array("first_name" => "Joe", "score" => 83, "last_name" => "Smith"),
    array("first_name" => "Frank", "score" => 92, "last_name" => "Barbson"),
    array("first_name" => "Benji", "score" => 90, "last_name" => "Warner")   
  );

  $freecodecamp = array("free", "code", "camp");
sort($freecodecamp);
print_r($freecodecamp);



?>